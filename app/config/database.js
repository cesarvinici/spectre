const sqlite3 = require('sqlite3').verbose();
const db = new sqlite3.Database('data.db');
CREATE_TABLE_MEMBERS = 'CREATE TABLE IF NOT EXISTS Members \
    (Member_ID INTEGER PRIMARY KEY AUTOINCREMENT, Name varchar(255) UNIQUE, Vocation varchar(255) not null, \
    Level int not null, Rank varchar(100) not null, Joined date not null, Ativo INTEGER not null, Status varchar(200), Obs varchar(255), Retorno INTEGER)';

CREATE_TABLE_GUILDBANK = 'CREATE TABLE IF NOT EXISTS Guild_Bank ( \
                           Member_ID integer, Valor DECIMAL(10,2) not null, Mes INTEGER not null, Ano INTEGER not null, Obs varchar(300), Created_at datetime, Updated_at datetime)' 


CREATE_TABLE_PAGT_ADICIONAL = 'CREATE TABLE IF NOT EXISTS GB_pagamento_adicional (Pagamento_ID INTEGER PRIMARY KEY AUTOINCREMENT, Member_ID INTEGER NOT NULL, Valor INTEGER NOT NULL, Obs varchar(255), Created_at datetime, Updated_at datetime) ';


db.serialize((err) => {
    db.run(CREATE_TABLE_MEMBERS);
    db.run(CREATE_TABLE_GUILDBANK);
    db.run(CREATE_TABLE_PAGT_ADICIONAL);
})


// SELECT lower(replace(Name, ' ', '')) as user from Members WHERE user = 'xekshrek'

module.exports = db;