module.exports = {
    verify_token: (req, res, next) => {
        if(req.originalUrl == '/login') return next();
        var token = req.body.token || req.query.token || req.headers['x-access-token'];
        if (token) {
            jwt.verify(token, 'URMBrmfZuy5ncrx', function (err, decoded) {
                if (err) {
                    res.json({ success: false, message: 'Falha ao tentar autenticar o token!' });
                } else {
                    //se tudo correr bem, salvar a requisição para o uso em outras rotas
                    req.decoded = decoded;
                    next();
                }
            });
        } else {
            // se não tiver o token, retornar o erro 403
            return res.status(403).send({
                success: false,
                message: '403 - Forbidden'
            });
        }
    }
}