
class Membro {

    constructor(db) {
        this._db = db;
    }

    lista() {
        return new Promise((resolve, reject) => {
            this._db.all('SELECT * from Members', (error, result) => {
                if (error) {
                    return reject("Ocorreu um erro: " + error)
                }

                return resolve(result);
            })
        })
    }

    busca(Member_ID){
        return new Promise((resolve, reject) => {
            this._db.get('SELECT * from Members WHERE Member_ID = ?', [Member_ID], (error, result) => {
                if (error) {
                    return reject("Ocorreu um erro: " + error)
                }

                return resolve(result);
            })
        })
    }
    
    buscaPorNome(nome){
        return new Promise((resolve, reject) => {
            this._db.get("SELECT Member_ID, Name, Joined from Members where Name = ?", [nome], (error, result) => {
                if (error) {
                    return reject("Ocorreu um erro: " + error)
                }

                return resolve(result);
            })
        })
    }

    update(Member_ID, dados){
        return new Promise((resolve, reject) => {
            var query = this._db.prepare('UPDATE Members SET Name = ?, Vocation = ?, Level = ?, Rank = ?, \
                     Joined = ?, Ativo = ?, Status = ?, Obs = ?, Retorno = ? WHERE Member_ID = ?');
            query.run([
                dados.Name,
                dados.Vocation,
                dados.Level,
                dados.Rank,
                dados.Joined,
                dados.Ativo,
                dados.Status,
                dados.Obs,
                dados.Retorno,
                Member_ID
            ],
                (err) => {
                    if (err) {
                        return reject(err);
                    }
                })
            query.finalize();
            return resolve('1');

        })
    }

    lideres() {
        return new Promise((resolve, reject) => {
            this._db.all('SELECT * from Members where Rank = "Boss"', (error, result) => {
                if (error) {
                    return reject("Ocorreu um erro: " + error)
                }

                return resolve(result);
            })
        })
    }

    integracao(membros) {
        return new Promise((resolve, reject) => {
            // Inativa todos os Membros
            this._db.run('UPDATE members set Ativo = 0')
            // Prepara o insert de novos membros
            var query = this._db.prepare('INSERT INTO Members (Name, Vocation, Level, Rank, Joined, Ativo, Status, Obs, Retorno) values (?, ?, ?, ?, ?, 1, null, null, null)');
            membros.forEach(membro => {
                // Verifica se o membro já existe no Banco
                this._db.each('SELECT * from Members where Name = ?', [membro.name], (err, row) => {

                    if (row) {
                        //Caso já exista é ativado
                        this._db.run('UPDATE Members set Ativo = 1, Level = ?, Rank = ?,  Joined = ? where Member_ID = ?', [ membro.level, membro.rank, membro.joined ,row.Member_ID]);
                    } else {
                        // Caso não exista ele é inativado
                        query.run([membro.name, membro.vocation, membro.level, membro.rank, membro.joined], (err) => {
                            if (err) {
                                return reject(err);
                            }
                        })
                    }
                })

            });
            query.finalize();
            return resolve()
        })
    }

    importaCSV(player) {
        return new Promise((resolve, reject) => {
            var query = this._db.prepare('INSERT INTO Members (Name, Vocation, Level, Rank, Joined, Ativo, Status, Obs, Retorno) \
                                                        values (?, "No Voc", "No LVL", "No Rank", "2018-12-01", 0, ?, ?, ?)');
             this._db.get('SELECT * from Members where Name = ?', [player.Char], (err, row) => {
                var retorno = player.Retorno != '' ? 0 : 1;
                if (err) {
                    return console.error("error!");
                }
    
                if (row) {
                    this._db.run('UPDATE Members set Status = ? , Obs = ? , Retorno = ? where Member_ID = ?', [player.Acao, player.Obs, retorno, row.Member_ID], (err) => {return reject(err)});
                } else {
                    // Caso não exista ele é inativado
                    query.run([player.Char, player.Acao, player.Obs, retorno], (err) => {
                        if (err) {
                            return reject(err)
                        }

                        
                    })
                }
                query.finalize();
            })
            
            return resolve();
        })
    }

    async get_dados_login(dados){
    
        return new Promise((resolve, reject) => {
            try {
                this._db.get("SELECT Member_ID, Name, Password, lower(replace(Name, ' ', '_')) as user from Members where user = ?", [dados.user], (err, row) => {
                    if(err){
                        return reject(error)
                    }
                    return resolve(row);
                });
            } catch (error) {
                return reject(error)
            }
        });
    }

}
module.exports = Membro