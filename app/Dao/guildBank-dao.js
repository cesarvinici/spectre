class GuildBank{
    constructor(db){
        this._db = db;
    }

    pagamento_membro(dados){
        return new Promise((resolve, reject) => {
            var query = this._db.prepare('INSERT into Guild_Bank (Member_ID, Valor, Mes, Ano, Obs, Created_at, Updated_at) values (?, ?, ?, ?, ?, datetime("now"), datetime("now"))');
            query.run([
                dados.Member_ID,
                dados.Valor,
                dados.Mes,
                dados.Ano,
                dados.Obs
            ],
                (err) => {
                    if (err) {
                        return reject(err);
                    }
                })
            query.finalize();
            return resolve('1');
        })
    }

    pagamentoAdicional(dados){
        return new Promise((resolve, reject) => {
            var query = this._db.prepare('INSERT into GB_pagamento_adicional (Member_ID, Valor, Created_at, Updated_at) values (?, ?, datetime("now"), datetime("now"))');
            query.run([
                dados.Member_ID,
                dados.Valor
            ],
                (err) => {
                    if (err) {
                        return reject(err);
                    }
                })
            query.finalize();
            return resolve('1');
        })
    }


    saldo_depositos(){
        return new Promise((resolve, reject) => {
            this._db.get('SELECT sum(Valor) as Total from Guild_Bank', (error, result) => {
                if (error) {
                    return reject("Ocorreu um erro: " + error)
                }

                return resolve(result.Total);
            })
        })
    }

    saldo_adicionais(){
        return new Promise((resolve, reject) => {
            this._db.get('SELECT sum(Valor) as Total from GB_pagamento_adicional', (error, result) => {
                if (error) {
                    return reject("Ocorreu um erro: " + error)
                }

                return resolve(result.Total);
            })
        })
    }

    busca_ultimo_pagamento(Member_ID){
        return new Promise((resolve, reject) => {
            try{
                let date = new Date()
                this._db.get('SELECT max(Mes) [Mes], max(Ano) [Ano] FROM Guild_Bank where Member_ID = ? and Ano = (SELECT max(Ano) from Guild_Bank WHERE Member_ID = ?)', [
                    Member_ID,
                    Member_ID
                    ] , (error, result) => {
                    if (error) {
                        console.log(error);
                        return reject();
                    }
                    return resolve([result.Mes, result.Ano]);
                })
            }catch(err){
                console.log(err);
                return reject('Ocorreu um erro!')
            }
        })
    }

    busca_historico_pagamentos(Member_ID){
        return new Promise((resolve, reject) => {
            try{
                let date = new Date()
                this._db.all('SELECT Valor, Mes, Ano, Obs FROM Guild_Bank where Member_ID = ? order by Ano, Mes ASC', [
                    Member_ID,
                    ] , (error, result) => {
                    if (error) {
                        console.log(error);
                        return reject();
                    }
                    return resolve(result);
                })
            }catch(err){
                console.log(err);
                return reject('Ocorreu um erro!')
            }
        })
    }



}

module.exports = GuildBank