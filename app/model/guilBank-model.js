const db = require('../config/database');
const Member = require('../model/member-model');
const GuildBankDao = require('../Dao/guildBank-dao');
const numeral = require('numeral');



 /**
 * ENTROU ANTES DO DIA 15 - PAGA O MÊS ATUAL
 * ENTROU DEPOI DO DIA 15 - PAGA REFERENTE AO PRÓXIMO MÊS
 */

class GuildBank{

    constructor(){
        this.gb_dao = new GuildBankDao(db)
    }


    async realizar_pagamento(dados){
           
        let ultimo_mes_pago = await this.ultimo_mes_pago(dados.Member_ID)       
        let mes_inicio = parseInt(ultimo_mes_pago[0]) + 1;
        let ano_inicio = parseInt(ultimo_mes_pago[1]);
        let pago_ate = dados.Meses + mes_inicio
        
        for(var i = mes_inicio; i <= pago_ate; i++){
            let dados_pagt = {
                Member_ID: dados.Member_ID,
                Valor: 40,
                Mes: i > 12 ? (i - 12) : i,
                Ano: i > 12 ? (ano_inicio + 1) : ano_inicio,
                Obs: dados.Obs
            }         
            await this.gb_dao.pagamento_membro(dados_pagt);
        }
    }

    async ultimo_mes_pago(Member_ID){
        return await this.gb_dao.busca_ultimo_pagamento(Member_ID)       
    }


    async historico_pagamentos(Member_ID){
        return await this.gb_dao.busca_historico_pagamentos(Member_ID);
    }

    async saldo_guild_bank(){
        let saldo_gb = await this.gb_dao.saldo_depositos();
        let saldo_adicional = await this.gb_dao.saldo_adicionais();
        let total_gb = saldo_adicional + parseFloat(saldo_gb.toFixed(2));
        return total_gb;
    }

    async importar_planilha_gb(guild){
        var total = 0.0;

        for (let index = 0; index < guild.length; index++) {
            const element = guild[index];
            var marco = parseFloat(element['Mar/19']) ? parseFloat(element['Mar/19']) : 0
            var abril = parseFloat(element['Apr/19']) ? parseFloat(element['Apr/19']) : 0
            var maio = parseFloat(element['May/19']) ? parseFloat(element['May/19']) : 0
            var junho = parseFloat(element['Jun/19']) ? parseFloat(element['Jun/19']) : 0
            var julho = parseFloat(element['Jul/19']) ? parseFloat(element['Jul/19']) : 0
            var adicional = parseFloat(element['Depósitos adicionais']) ? (numeral(element['Depósitos adicionais']).value() / 1000) : 0
            var saldo = (marco + abril + maio + junho + julho)
            total += parseFloat(saldo);
            if (index == 0) {
                var total = marco + abril
                await this.gb_dao.pagamento_membro({ Valor: marco, Mes: 3, Ano: 2019, Obs: 'Pagamento GB' });
                await this.gb_dao.pagamento_membro({ Valor: abril, Mes: 4, Ano: 2019, Obs: 'Pagamento GB' });
                continue;
    
            }
            var member = new Member();   
            var nome = element['Player'].replace(/\s/g, ' ');
            var player = await member.busca_por_nome(nome);

            if (adicional) {
                var dados_pagt_adicional = {
                    Member_ID: player.Member_ID,
                    Valor: adicional,
                }
                await this.gb_dao.pagamentoAdicional(dados_pagt_adicional);
            }

            if (marco) {
                var dados_guild_bank_marco = {
                    Member_ID: player.Member_ID,
                    Valor: VALOR_GB,
                    Mes: 3,
                    Ano: 2019,
                    Obs: element['Obs']
                }

                await this.gb_dao.pagamento_membro(dados_guild_bank_marco);
            }

            if (abril) {
                var dados_guild_bank_abril = {
                    Member_ID: player.Member_ID,
                    Valor: VALOR_GB,
                    Mes: 4,
                    Ano: 2019,
                    Obs: element['Obs']
                }

                await this.gb_dao.pagamento_membro(dados_guild_bank_abril);

            }
            if (maio) {
                var dados_guild_bank_maio = {
                    Member_ID: player.Member_ID,
                    Valor: VALOR_GB,
                    Mes: 5,
                    Ano: 2019,
                    Obs: element['Obs']
                }

                await this.gb_dao.pagamento_membro(dados_guild_bank_maio);

            }
            if (junho) {
                var dados_guild_bank_junho = {
                    Member_ID: player.Member_ID,
                    Valor: VALOR_GB,
                    Mes: 6,
                    Ano: 2019,
                    Obs: element['Obs']
                }

                await this.gb_dao.pagamento_membro(dados_guild_bank_junho);

            }
            if (julho) {
                var dados_guild_bank_julho = {
                    Member_ID: player.Member_ID,
                    Valor: VALOR_GB,
                    Mes: 7,
                    Ano: 2019,
                    Obs: element['Obs']
                }

                await this.gb_dao.pagamento_membro(dados_guild_bank_julho);
            }
        }

        console.log(total.toFixed(2));
    }

}

module.exports = GuildBank