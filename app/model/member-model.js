const db = require('../config/database');
const MemberDao = require('../Dao/membro-dao');
const bcrypt = require('bcrypt');
const request = require('request');
/**
 * @author Cesar Vinicius <cesarvinici@gmail.com>
 * @class Member
 * @date 14/04/2019
 */
class Member {

    constructor() {
        this.memberDao = new MemberDao(db);
        this._saltRounds = 10;
    }
    /**
     */
    async lista() {
        return await this.memberDao.lista();
    }
    /**
     * @param  {int} Membro_ID
     */
    async busca(Membro_ID) {
        return await this.memberDao.busca(Membro_ID);
    }
    /**
     * @param  {} Nome
     * 
     */
    async busca_por_nome(Nome) {
        return await this.memberDao.buscaPorNome(Nome)
    }

    async lideres() {
        return await this.memberDao.lideres();
    }

    async update(Membro_ID, dados){
        return await this.memberDao.update(Membro_ID, dados);
    }
    /**
     * @param  {array} dados_login
     * @returns {array} token
     */
    async login(dados_login){
        
        let dados_db = await this.memberDao.get_dados_login(dados_login)
        let token = null;
        if(dados_db && dados_db.Member_ID){
            //Caso password correto, return true
            if(await this.compare_pass(dados_login.password, dados_db.Password)){
                token = {
                    token: jwt.sign({  user: dados_db.Name, 
                        id: dados_db.Member_ID,
                    }, 'URMBrmfZuy5ncrx', {
                        expiresIn: '1h'
                    }),
                    alterar_senha: 0,
                    logado: 1
                }

                //Caso password for igual o usuário retorna 2 para redirecionar para troca de pdassword
                if(await this.compare_pass(dados_db.user, dados_db.Password)){
                    token['alterar_senha'] = 1
                }
            }
            
            if(dados_db.Password == null && dados_login.password == dados_db.user){
                //Precisa torcar o password

                token = {
                    token: jwt.sign({  user: dados_db.Name, 
                        id: dados_db.Member_ID,
                    }, 'URMBrmfZuy5ncrx', {
                        expiresIn: '1h'
                    }),
                    alterar_senha: 1,
                    logado: 1
                }
            }
        }
        //Usuário incorreto
        
        return token;
    }


    async integracao() {
        var membros = []
        let ranks = []
        let array_membros = await new Promise((resolve, reject) => {
            try {
                request('https://api.tibiadata.com/v2/guild/Spectre.json', { json: true }, (err, res, body) => {

                    for (var i = 0; i < body.guild.members.length; i++) {
                        body.guild.members[i]['characters'].forEach(membro => {
                            membro['rank'] = body.guild.members[i]['rank_title'];
                            membros.push(membro);
                        });

                        ranks.push(body.guild.members[i]['rank_title']);
                    }
                    if (err) { throw new Error(err) }
                    return resolve(membros)
                })
            } catch (error) {
                return reject(error)
            }
        })
       
       await this.memberDao.integracao(array_membros);
    }

    /**
     * 
     * @param {string} pass
     * @returns {string}  
     */
   async encrypt_pass(pass){
        let password = await new Promise((resolve, reject) => {
            bcrypt.hash(pass, 10, (err, hash) =>{
                if(err){
                    return reject(err)
                }
    
                return resolve(hash);
            });
        })
        return password;
    }

    async compare_pass(pass_informado, db_pass){
        if(!db_pass) return false;

        let match = await bcrypt.compare(pass_informado, db_pass);

        return match;

    }


}

module.exports = Member