const express = require('express')
const app = express();
const fs = require('fs');
global.jwt = require('jsonwebtoken');
const csv = require('csv-parser');
const db = require('./app/config/database')
const Member = require('./app/model/member-model');
const bodyParser = require('body-parser');
const GuildBank = require('./app/model/guilBank-model');
const session = require('express-session');
const middlewares = require('./app/config/jwt');
global.VALOR_GB = 40;


app.use(bodyParser.json());
app.use(middlewares.verify_token);


/** GET INTEGRA ARQUIVO EX MEMBROS.CSV */
app.get('/csv', async (req, res) => {

    dados = await lerCSV();
    membro = new Member(db);
    for (let index = 0; index < dados.length; index++) {
        const element = dados[index];
        membro.importaCSV(element);
    }

    console.log("Planilha Importada!");

})
/** GET ALL MEMBROS  */
app.get('/', async (req, res) => {

    const member = new Member();
    console.log(req.decoded);
    try {
        membros = await member.lista();
        res.json(membros).status(200);
    } catch (err) {
        console.log(err);
    }
});

/** GET MEMBRO */
app.get('/membro/(:membroId)', async (req, res) => {
    const member = new Member();
    try {
        membro = await member.busca(req.params.membroId);
        res.json(membro).status(200);
    } catch (err) {
        console.log(err);
    }
})

/** POST MEMBRO */
app.post('/membro/(:membroId)', async (req, res) => {
    const member = new Member();
    try {
        if (req.body.Member_ID != parseInt(req.params.membroId)) throw new Error('Atividade não autorizada!');

        membro = await member.update(req.params.membroId, req.body);
        res.json({ mensagem: "Cadastro alterado com sucesso!", class: 'success' }).status(200);
    } catch (err) {
        res.json({ mensagem: "Ocorreu um erro: " + err.message, class: 'danger' }).status(500);
    }
})

/** GET RETURN LIDERES */
app.get('/lideres', async (req, res) => {

    const member = new Member();

    try {
        membros = await member.lideres();
        res.json(membros).status(200);
    } catch (err) {
        res.json({ mensagem: "Ocorreu um erro: " + err.message, class: 'danger' }).status(500);
    }

});

/**
 * GET saldo guild bank
 */
app.get('/saldo_guild_bank', async (req, res) => {

    try {
        gb = new GuildBank();
        let saldo = await gb.saldo_guild_bank();
        res.send({ saldo: saldo });
    } catch (err) {
        res.json({ mensagem: "Ocorreu um erro: " + err.message }).status(500);
    }
})

/**
 * POST - Lança pagamento de um mebro
 */
app.post('/realizar_pagamento_guildBank/', async (req, res) => {

    try {
        gb_model = new GuildBank();
        let dados = {
            Member_ID: req.body.Member_ID,
            Meses: req.body.Meses,
            Obs: req.body.Obs
        }
        await gb_model.realizar_pagamento(dados);
        res.json({ mensagem: 'Pagamento lançado com sucesso', class: 'success' }).status(200);
    } catch (err) {
        res.json({ mensagem: "Ocorreu um erro: " + err.message, class: 'danger' }).status(500);
    }
})

/**
 * GET retorna histórico de pagamentos do GB
 */
app.get('/pagamentos_membro/(:membroId)', async (req, res) => {
    try {
        Member_ID = parseInt(req.params.membroId);

        if (!Member_ID) throw new Error('Membro ID incorreto');

        gb_model = new GuildBank();
        historico_gb = await gb_model.historico_pagamentos(Member_ID);
        res.json({ historico: historico_gb }).status(200);
    } catch (err) {
        res.json({ mensagem: "Ocorreu um erro: " + err.message }).status(500);
    }
})

/**
 * Importa planilha guildbank.csv para o sistema
 */
app.get('/importar_guildbank', async (req, res) => {
    try {
        //ler csv guildbank
        guild = await lerCSVGuildBank();
        gb_model = new GuildBank();
        await gb_model.importar_planilha_gb(guild)
        res.json({ mensagem: 'Guild Bank importado com sucesso!', class: 'success' }).status(200);
    } catch (err) {
        res.json({ mensagem: "Ocorreu um erro ao importar a Planilha de GuildBank - " + err.message, class: 'danger' }).status(200);
    }
});

/**
 * GET
 * Integra API do tibia com banco local
 */
app.get('/integracao', async (req, res) => {
    try {
        member = new Member();
        await member.integracao();
        res.json({ mensagem: 'Integração realizada com sucesso!', class: 'success' }).status(200);

    } catch (err) {
        res.json({ mensagem: "A Integração não pode ser realizada - " + err.message, class: 'danger' }).status(200);

    }
});

/**
 * POST - Login de usuário
 */
app.post('/login', async (req, res) => {

    try {
        var dados_login = {
            user: req.body.user,
            password: req.body.password
        }

        let member = new Member();
        let dados = await member.login(dados_login);
        if (!dados) {
            return res.json({ mensagem: 'Usuário ou senha incorretos', class: 'warning' }).status(500);
        }
        res.json(dados).status(200);
    } catch (err) {
        console.log(err);
        res.json({ mensagem: "Ocorreu um erro: " + err.message, class: 'danger' }).status(500);
    }
})

app.listen(3000, () => console.log('Ouvindo na porta 3000'));



//LEITURA DOS ARQUIVOS CSV 

/**
 * Le arquivo guildBank.csv
 */
function lerCSVGuildBank() {
    return new Promise((resolve, reject) => {
        fs.createReadStream('guildbank.csv')
            .pipe(csv())
            .on('data', (data) => results.push(data))
            .on('end', () => {
                resolve(results);
            });
    })
}



function lerCSV() {
    var results = [];
    return new Promise((resolve, reject) => {
        fs.createReadStream('spectre.csv')
            .pipe(csv())
            .on('data', (data) => results.push(data))
            .on('end', () => {
                resolve(results);
            });
    })
}